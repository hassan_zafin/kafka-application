package com.capstage.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.capstage.controller.KafkaController;
import com.capstage.controller.KafkaController2;
import com.capstage.entities.Message;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(KafkaController.class)
public class TestKafkaController {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	KafkaController controller;

	@Test
	void shouldCreatMockMvc() {
		assertNotNull(mockMvc);
	}

	@Test
	public void getAllMessagesAPI() throws Exception {
		Message msg1 = new Message("content1", "date1");
		Message msg2 = new Message("content2", "date2");
		doReturn(Lists.newArrayList(msg1, msg2)).when(controller).showMessages();
		mockMvc.perform(MockMvcRequestBuilders.get("/postings/")
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$").exists())
				.andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].content", is("content1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].date", is("date1")))
				
				
				;
	}

	@Test
	public void PostMessageAPI() throws Exception {
		 mockMvc.perform(MockMvcRequestBuilders.post("/postings/")
				.content(asJsonString(new Message("This is the content", "This is the date")))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].content").exists())
				;

	}
	
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
	 

}
