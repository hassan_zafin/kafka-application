package com.capstage.controller;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.common.PartitionInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capstage.entities.Message;
import com.capstage.service.ConsB;
import com.capstage.service.ProdA;
import com.capstage.service.Topics;
import com.capstage.storage.MsgStore;

//@JsonIgnoreType
@RestController
@RequestMapping("/postings")
@CrossOrigin(origins = { "http://localhost:4200", "http://localhost:9876" })

public class PostingsController {

	Properties props = new Properties();
	@Autowired
	ProdA producer;
	@Autowired
	ConsB consumer;

	@Autowired
	private StreamBridge streamBridge;
	MsgStore msgStore = new MsgStore();

	@PostMapping(value = "/{topic}")
	public ResponseEntity<Message> publish(@PathVariable String topic, @RequestBody Message msg) {
		msg.setTopic(topic);

		props.put("spring.cloud.stream.bindings.messageConsumer-in-0.destination", topic);
		msgStore.getMessages().add(msg);
		streamBridge.send(topic, msg);
		// System.err.println(msg.fromObjectToString());
		return new ResponseEntity<Message>(msg, HttpStatus.CREATED);

	}

	@GetMapping(value = "/topics/{name}/{partitions}/{replicas}")
	public void createTopic(@PathVariable String name, @PathVariable int partitions, @PathVariable int replicas)
			throws InterruptedException, ExecutionException {
		
		new Topics().createTopic(name, partitions, replicas);
	}

//	@GetMapping(value = "/delete/{topic}")
//	public void deleteTopics(@PathVariable String topic) throws InterruptedException, ExecutionException {
//		//new Topics().deleteTopic(topic);
//		new Topics().describe(topic);
//	
//	}

	@GetMapping(value = "/{topic}")
	public List<Message> showMessagesByTopic(@PathVariable String topic) {
		return msgStore.getMessages();
	}

	@GetMapping(value = "/")
	public List<Message> showMessages() {
		return msgStore.getMessages();
	}

	@GetMapping(value = "/topics")
	public Set<String> getListTopics() throws InterruptedException, ExecutionException {
		return new Topics().getTopicsAsSet();
	}
	@GetMapping(value="/topics/delete/{name}") 
	public void deleteTopic(@PathVariable String name) {
		new Topics().deleteTopic(name);
	}

	@GetMapping(value = "/topics/later")
	public List<List<String>> getTopicInfo(@PathVariable String name) throws InterruptedException, ExecutionException {
		return new Topics().getPartitions(name);
	}

	@GetMapping(value = "/topices/{name}")
	public int getNumberOfPartitions(@PathVariable String name) {
		return new Topics().getNumberOfPartitions(name);
	}

}
