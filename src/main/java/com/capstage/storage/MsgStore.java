package com.capstage.storage;

import java.util.ArrayList;
import java.util.List;

import com.capstage.entities.Message;

public class MsgStore {

	private static List<Message> messages = new ArrayList<Message>();
	
	public List<Message> getMessages() {
		return messages;
	}
	
}
