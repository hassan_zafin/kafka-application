package com.capstage.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.capstage.entities.Message;

@Service
public class ConsAProdB {

	public static final String TOPIC_CONS = "topA";
	public static final String TOPIC_PROD = "topB";
	@Autowired
	private KafkaTemplate<String,String> kafkaTemp;
	
	public void publish(String msg) {
		this.kafkaTemp.send(TOPIC_PROD,msg);
	}
	
	@KafkaListener(topics=TOPIC_CONS, groupId="mygroup")
	public void consume(String msg) {
		
		System.out.println("J'ai consomme depuis A : "+msg);
		
		publish(msg);
	}
	
	
}

