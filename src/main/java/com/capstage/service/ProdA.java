package com.capstage.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.capstage.entities.Message;

@Service
public class ProdA {
	public static final String TOPIC_PROD = "topA";
	@Autowired
	private KafkaTemplate<String,String> kafkaTemp;
	
	public void publish(Message msg) {
		this.kafkaTemp.send(TOPIC_PROD,msg.fromObjectToString());
	}
	
	
}
