package com.capstage.service;

import java.util.concurrent.CountDownLatch;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class ConsB {
	public static final String TOPIC_CONS = "topB";
	private CountDownLatch latch = new CountDownLatch(1);

	public CountDownLatch getLatch() {
		return latch;
	}

	@KafkaListener(topics = TOPIC_CONS, groupId = "mygroup")
	public void consume(String msg) {
		//messages.add(msg);
		System.out.println("J'ai consomme depuis B : " + msg);
		latch.countDown();
	}

	
}
