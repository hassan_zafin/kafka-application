package com.capstage.service;

import java.util.function.Consumer;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.capstage.entities.Message;

@Service
public class PostingsService {
	
	@Bean
	public Consumer<Message> messageConsumer() {
		return (input) -> {
			System.err.println("***********************************");
			System.out.println(input.toString());
			System.err.println("***********************************");
		};
	}

}
