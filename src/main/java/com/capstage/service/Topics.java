package com.capstage.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.PartitionInfo;
import org.springframework.kafka.core.KafkaAdmin.NewTopics;

public class Topics {
	Map<String, List<PartitionInfo>> topics;

	static Properties props = new Properties();
	
	static {
		props.put("bootstrap.servers", "localhost:9092");
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
	}
	public int getNumberOfPartitions(String topic) {
		//KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
		Producer producer = new KafkaProducer(props);
		
		return producer.partitionsFor(topic).size();
		
	}
	
	public void createTopic(String name,int partitions, int replicas) throws InterruptedException, ExecutionException {
		
		AdminClient admin = AdminClient.create(props);
		List<NewTopic> list = new ArrayList<NewTopic>();
		NewTopic tp = new NewTopic(name,partitions,(short) replicas);
		list.add(tp);
		admin.createTopics(list);
	}
	
	public void deleteTopic(String topic) {
		List<String> list = new ArrayList<String>();
		list.add(topic);
		AdminClient admin = AdminClient.create(props);
		admin.deleteTopics(list);
	}

	public List<List<String>>  getPartitions(String topic) {
		List<List<String>> list = new ArrayList<>();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
		List<PartitionInfo> partition_list= consumer.partitionsFor(topic);
		
		for(PartitionInfo s : partition_list) {
			String partition_id = String.valueOf(s.partition());
			String leader =String.valueOf(s.leader().id()); 
			String replicas = String.valueOf(s.replicas().length); 
			List<String> ts = new ArrayList<String>();
			ts.add(partition_id); ts.add(leader); ts.add(replicas);
			list.add(ts);
		}
		return list;
	}
	
	
	
	public Map<String, List<PartitionInfo>> getTopics() {
		
		props.put("bootstrap.servers", "localhost:9092");
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
		topics = consumer.listTopics();
		//System.out.println("________________________");
		//System.out.println(topics.size());
		//System.out.println(topics.keySet());
		//System.out.println("________________________");
		//System.out.println(consumer.partitionsFor("topB").toString());
		System.err.println(consumer.partitionsFor("topA"));
		consumer.close();
		return topics;
		

	}
	
	
	public Set<String> getTopicsAsSet() throws InterruptedException, ExecutionException {
		Properties properties = new Properties();
		properties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		AdminClient adminClient = AdminClient.create(properties);
		ListTopicsOptions listTopicsOptions = new ListTopicsOptions();
		listTopicsOptions.listInternal(true);
		//System.out.println(adminClient.listTopics(listTopicsOptions).names().get());
		return adminClient.listTopics().names().get();
	}

}
