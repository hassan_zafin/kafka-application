package com.capstage.entities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Message {
	private String topic = "";

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	private String content = "";
	// private String date =
	// LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy
	// HH:mm:ss"));
	private String date;

	public Message() {
		super();

	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Message(String content, String date, String topic) {
		super();
		this.content = content;
		this.date = date;
		this.topic = topic;
	}

	public String fromObjectToString() {
		String obj = content + ":" + date + " topic : " + topic;
		return obj;
	}

}
