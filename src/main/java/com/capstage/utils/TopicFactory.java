package com.capstage.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;


@Configuration
public class TopicFactory {
	int partitions = 1 ;
	String topic_name = "lololo";
	int replicas = 1;
	
	public int getPartitions() {
		return partitions;
	}

	public void setPartitions(int partitions) {
		this.partitions = partitions;
	}

	public String getTopic_name() {
		return topic_name;
	}

	public void setTopic_name(String topic_name) {
		this.topic_name = topic_name;
	}

	public int getReplicas() {
		return replicas;
	}

	public void setReplicas(int replicas) {
		this.replicas = replicas;
	}



	
	
	@Bean
	public NewTopic createTopic() {
		
		return TopicBuilder.name(topic_name)
				.partitions(partitions)
				.replicas(replicas)
				.build();	
	}
	
	



	
}
